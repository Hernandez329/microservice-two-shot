import React from "react";

class HatForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            fabric: '',
            color: '',
            picture: '',
            location: '',
            locations: [],
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleFabricChange = this.handleFabricChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
    };

    
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        delete data.locations;
        console.log(data)

        const hatsUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": 'applications/json'
            }
        };
        const hatResponse = await fetch(hatsUrl, fetchConfig);
        if (hatResponse.ok) {
            const newHat = await hatResponse.json();
            console.log(newHat)
            this.setState({
                name: '',
                fabric: '',
                color: '',
                picture: '',
                location: '',
            });
        }
    }
    

    handleFabricChange(event) {
        const value = event.target.value;
        this.setState({ fabric: value });
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value });
    }

    handleColorChange(event) {
        const value = event.target.value;
        this.setState({ color: value });
    }

    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({ picture: value });
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value });
    }

    async componentDidMount() {
        const url = "http://localhost:8100/api/locations";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new hat</h1>
                        <form onSubmit={this.handleSubmit} id="create-hat-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name} />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={this.state.fabric} />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={this.state.color} />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePictureChange} placeholder="Picture" required type="text" name="picture" id="picture" className="form-control" value={this.state.picture} />
                                <label htmlFor="Picture">Picture</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select">
                                    <option value="">Choose a location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.href}>
                                                {location.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>

        )

    }

}

export default HatForm;