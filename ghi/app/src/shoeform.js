import React from 'react';

class ShoeForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            manufacturer: '',
            color: '',
            pic: '',
            bins: []
        }
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this);
        this.handleColorChange = this.handleColorChange.bind(this);
        this.handlePicChange = this.handlePicChange.bind(this);
        this.handleBinChange = this.handleBinChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async handleSubmit(event){
        event.preventDefault()
        const data = {...this.state}
        delete data.bins

        const shoesUrl = 'http://localhost:8080/api/shoes/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        }
        const response = await fetch(shoesUrl, fetchConfig)
        if (response.ok){
            const newshoe = await response.json()
            console.log(newshoe)

            const clear = {
                name: '',
                manufacturer: '',
                color: '',
                pic: '',
                bin: ''
            }
            this.setState(clear)
        }


    }
    handleNameChange(event){
        const value = event.target.value
        this.setState({name: value})
    }
    handleManufacturerChange(event){
        const value = event.target.value
        this.setState({manufacturer: value})
    }
    handleColorChange(event){
        const value = event.target.value
        this.setState({color: value})
    }
    handlePicChange(event){
        const value = event.target.value
        this.setState({pic: value})
    }
    handleBinChange(event){
        const value = event.target.value
        this.setState({bin: value})
    }
    async componentDidMount(){
        const url = 'http://localhost:8100/api/bins/'
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json()
            this.setState({bins: data.bins})
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a Shoe</h1>
                        <form onSubmit={this.handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                <input value={this.state.name}
                                    onChange={this.handleNameChange} placeholder="name" required type="text" name="name" id="name"
                                    className="form-control" />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.manufacturer}
                                    onChange={this.handleManufacturerChange} required type="text" id="manufacturer" name="manufacturer" className="form-control" />
                                <label htmlFor="manufacturer">Manufacturer:</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.color}
                                    onChange={this.handleColorChange} required type="text" id="color" name="color" className="form-control" />
                                <label htmlFor="color">Color:</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input value={this.state.pic}
                                    onChange={this.handlePicChange} placeholder="pic" type="text" name="pic" id="pic" className="form-control" />
                                <label htmlFor="pic">Picture</label>
                            </div>
                            <div className="mb-3">
                                <select value={this.state.bin} onChange={this.handleBinChange}
                                    required name="bin" id="bin" className="form-select">
                                    <option value="">Bin</option>
                                    {this.state.bins.map(bin => {
                                        return (
                                            <option key={bin.href} value={bin.href}>
                                                {bin.closet_name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}
export default ShoeForm;