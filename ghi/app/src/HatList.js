import React from "react";

class HatList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hats: [],
        };
        this.handleDelete = this.handleDelete.bind(this);
    }
    async componentDidMount() {
        const url = "http://localhost:8090/api/hats/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ hats: data.hats });
            console.log(data.hats)
        }
    }
   
    async handleDelete(id) {
        fetch(`http://localhost:8090/api/hats/${id}`, {
            method: "DELETE"
        }).then((result)=>{
            result.json().then((resp)=>{
                console.warn(resp)
                window.location.reload()
            })
        })
    }

    render () {
        return ( 
            <>
            <table className="table table-striped">
            <thead>
              <tr>
                <th>Name</th>
                <th>Location</th>
                <th>Color</th>
                <th>Fabric</th>
                <th>Picture</th>
                <th>Shelf</th>
                <th>Options</th>
              </tr>
            </thead>
            <tbody>
              {this.state.hats.map(hat => {
                return (
                  <tr key={hat.href}>
                    <td>{hat.name}</td>
                    <td>{hat.location.closet_name}</td>
                    <td>{hat.fabric}</td>
                    <td>{hat.color}</td>
                    <td><img src={hat.picture}></img></td>
                    <td>{hat.location.shelf_number}</td>
                    <td><button onClick={()=>this.handleDelete(hat.id)} >Delete</button></td>
                    
                    
                  </tr>
                );
              })}
      
            </tbody>
          </table>
          </>
        
        )
    }
}

export default HatList;