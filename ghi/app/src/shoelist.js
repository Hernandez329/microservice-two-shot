import React from 'react';



class ShoeList extends React.Component {
    constructor(prop) {
        super(prop)
        this.state = {shoes: []}
        this.handleDelete = this.handleDelete.bind(this)
    }
    async componentDidMount() {
        const url = "http://localhost:8080/api/shoes/"
        const response = await fetch(url);
        if (response.ok);
        const data = await response.json();
        console.log(data)
        this.setState({ shoes: data.shoes }) 
    }
    async handleDelete(pk){
        fetch(`http://localhost:8080/api/shoes/${pk}`,{
            method:'DELETE'
        }).then((result)=>{
            result.json().then((resp)=>{
                console.warn(resp)
                window.location.reload()
            })
        })
        }
    render() {
        return (
            <div className="container">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Shoe Name</th>
                            <th>manufacturer</th>
                            <th>Color</th>
                            <th>Picture</th>
                            <th>Closet Name</th>
                            <th>Bin Number</th>
                            <th>Shoe begone</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.shoes.map(shoe => {
                            return (
                                <tr key={shoe.pk}>
                                    <td>{shoe.name}</td>
                                    <td>{shoe.manufacturer}</td>
                                    <td>{shoe.color}</td>
                                    <td>
                                        <img src={shoe.pic} />
                                    </td>
                                    <td>{shoe.bin.closet_name}</td>
                                    <td>{shoe.bin.bin_number}</td>
                                    <td>
                                        <button onClick={()=>this.handleDelete(shoe.pk)} >Delete</button>
                                    </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ShoeList;