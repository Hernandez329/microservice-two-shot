from django.contrib import admin
from .models import Location
# Register your models here.

from .models import Bin

admin.site.register(Bin)


class LocationAdmin(admin.ModelAdmin):
    pass

admin.site.register(Location, LocationAdmin)

