# Generated by Django 4.0.3 on 2022-09-09 05:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('shoes_rest', '0002_binvo_import_href'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shoes',
            name='bin',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='bins', to='shoes_rest.binvo'),
        ),
    ]
