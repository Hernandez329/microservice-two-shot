import json
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import BinVO, Shoes

class BinVoDetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "import_href",
    ]

class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "name",
        "manufacturer",
        "color",
        "pic",
        "bin",
        "pk"
    ]
    encoders = {
        "bin": BinVoDetailEncoder(),
    }
    
    

class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "name",
        "manufacturer",
        "color",
        "pic",
        "bin",
        ]
    encoders = {
        "bin": BinVoDetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def shoes_list(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin"},
                status=400,
            )
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET", "DELETE", "PUT"])
def shoes_detail(request, pk):
    if request.method == "GET":
        shoes = Shoes.objects.filter(id=pk)
        return JsonResponse(
            shoes, encoder=ShoesDetailEncoder, safe=False
        )
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content  = json.loads(request.body)
        try:
            bin_href = content["bin"]
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"] = bin
            # bin = BinVO.objects.get(id=pk)
            # content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        Shoes.objects.filter(id=pk).update(**content)
        shoes = Shoes.objects.get(id=pk)
        return JsonResponse(
            shoes, encoder=ShoesDetailEncoder,
            safe=False
        )




