from tkinter import CASCADE
from django.db import models

# Create your models here.



class BinVO(models.Model):
    closet_name = models.CharField(max_length=100)
    bin_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.closet_name



class Shoes(models.Model):
    name = models.CharField(max_length=100)
    manufacturer = models.CharField(max_length=100)
    color = models.CharField(max_length=100)
    pic = models.URLField()
    bin = models.ForeignKey(BinVO, related_name="bins", 
    on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name



