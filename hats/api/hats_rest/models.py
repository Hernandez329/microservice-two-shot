from django.db import models
from django.urls import reverse

# Create your models here.

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)

class Hat(models.Model):
    name = models.CharField(max_length=75)
    fabric = models.CharField(max_length=75)
    color = models.CharField(max_length=75)
    picture = models.URLField(null=True)
    location = models.ForeignKey(LocationVO,related_name='hats', on_delete=models.CASCADE)
    
    def __str__(self):
        return self.name
    
    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})