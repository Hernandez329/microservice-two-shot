from django.contrib import admin

from .models import Hat, LocationVO
# Register your models here.

class HatAdmin(admin.ModelAdmin):
    pass

class LocationVOAdmin(admin.ModelAdmin):
    pass

admin.site.register(LocationVO, LocationVOAdmin)
admin.site.register(Hat, HatAdmin)