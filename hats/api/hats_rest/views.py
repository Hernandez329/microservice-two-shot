from xml.parsers.expat import model
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO

# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["closet_name", "import_href", "section_number", "shelf_number", "id"]

class HatListEnconder(ModelEncoder):
    model = Hat
    properties = ["name", "fabric", "color", "picture", "location", "id"]
    encoders = {"location": LocationVODetailEncoder()}

    # def get_extra_data(self, o):
    #     return {
    #         "location": o.location.closet_name,
    #         "section_number": o.location.section_number,
    #         "shelf_number": o.location.shelf_number
    #         }

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ["name", "fabric", "color", "picture", "location"]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    if request.method == "GET":
        
        hats = Hat.objects.all()

        return JsonResponse(
            {'hats': hats},
            encoder=HatListEnconder,
        )
    else: 
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )

        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_hat(request, pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"delete": count > 0})
    
    else:
        content = json.loads(request.body)

        try:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location"},
                status=400,
            )
        Hat.objects.filter(id=pk).update(**content)

        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatDetailEncoder,
            safe=False,
        )
